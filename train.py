#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from tkinter.messagebox import NO
from datasets import sixp_dataset
from utils.utils import get_logger
from utils.train_utils import TBLog, simple_accuracy
from utils.vis_utils import visualize, visualize_labels


import torch.utils.data
import torch.nn.functional as F
import torch.optim as optim 

import segmentation_models_pytorch as smp

from tqdm import tqdm
from ipdb import set_trace as st
import numpy as np
import os

class TrainModel():

    def __init__(self, options):
        self.options = options
        self.checkpoints_dir = self.create_checkpoints_dir(options)
        self.tb_log = TBLog(self.checkpoints_dir, 'tensorboard')
        self.logger = get_logger(options.name, self.checkpoints_dir, level='INFO')
        self.print_fn = self.logger.info
        self.iter = 0 # TO DO: Find a better way to count iterations.

    def trainProcess(self):

        # Define the network to be used
        net = self._get_network()
        net.cuda()
        # Define the optimizer
        optimizer = self._get_optimizer(net)
        # Define the dataset
        dset = self._get_dataset()
        dataloader = torch.utils.data.DataLoader(dset, batch_size=self.options.batch_size)

        print('\n Training ... \n')

        if self.options.start_epoch > 1:
            checkpoint_path = os.path.join(self.options.checkpoints_dir, self.options.name, 'latest.pth.tar')

            net.load_state_dict(torch.load(checkpoint_path, map_location=lambda storage, loc: storage)['model_state_dict'])
            optimizer.load_state_dict(torch.load(checkpoint_path, map_location=lambda storage, loc: storage)['optimizer_state_dict'])   

        for epoch in range(self.options.start_epoch, self.options.nEpochs+1):
            # Compute a training epoch
            self.trainEpoch(dataloader, net, optimizer, epoch, self.iter)
    
            if epoch % self.options.save_freq == 0:
                save_name = 'epoch_{:04}.pth'.format(epoch)
                self._save_checkpoint(save_name, self.checkpoints_dir, net, optimizer, epoch) 
            
             
    def trainEpoch(self, dataloader, net, optimizer, epoch, iter):
        start_epoch = torch.cuda.Event(enable_timing=True)
        end_epoch = torch.cuda.Event(enable_timing=True)
 
        net.train()
        start_epoch.record()
        for batch_idx, (data, target) in enumerate(dataloader):
            data, target = data.cuda(), target.cuda()
            optimizer.zero_grad()
            output = net(data)

            cross_entropy_loss = F.cross_entropy(output, target)
            cross_entropy_loss.backward()

            optimizer.step()

            tb_dict = {}
            tb_dict['train/sup_loss'] = cross_entropy_loss.detach() 
            tb_dict['lr'] = optimizer.param_groups[0]['lr']
            tb_dict['train/accuracy'] = simple_accuracy(torch.argmax(output, dim=1), target)

            tb_img_dict = {}

            if (self.options.print_info_freq > 0) & (self.iter % self.options.print_info_freq == 0):
                self.print_fn(tb_dict)

            if not self.tb_log is None:
                self.tb_log.update(tb_dict, self.iter)

                if (self.options.display_freq > 0) & (self.iter % self.options.display_freq == 0):
                    img_dict = self._get_images(data, None, target, output)
                    tb_img_dict.update(img_dict)
                    self.tb_log.update_imgs(tb_img_dict, it=self.iter)
            
            self.iter +=1
        end_epoch.record()
        
        torch.cuda.synchronize()
        print('Time per epoch : ', start_epoch.elapsed_time(end_epoch)/1000)
         

    def create_checkpoints_dir(self, options):
        checkpoint_path = os.path.join(options.checkpoints_dir, options.name) 
        os.makedirs(checkpoint_path, exist_ok=True)
        return checkpoint_path

    def _get_network(self):
        net_arch = self.options.net_architecture
        in_channels = self.options.input_nc
        out_channels_sup = self.options.n_classes

        if net_arch == 'unet':
            return smp.Unet(encoder_name='resnet50', encoder_weights=None, in_channels=in_channels, classes=out_channels_sup)
        else:
            raise NotImplementedError("Your input network is not supported (yet)")

    def _get_optimizer(self, net):
        # For now we will only use Adam optimizer
        # TO DO: Add other options to choose the optimizer
        return optim.Adam(net.parameters(), lr=self.options.lr)

    def _get_dataset(self):
        # dataset to be used [SixP]
        dataset = self.options.dataset

        if dataset == 'sixp':
            lb_dataset = sixp_dataset.sixP(self.options.data_root, normalize=True)
            return lb_dataset        
        else:
            raise NameError("Dataset must be SixP. Others need to be implemented.")

    def _get_images(self, x_lb, x_ulb, y_lb, preds):
        img_dict = {}
        img_dict['images'] = visualize(x_lb)
        if x_ulb is not None:
            img_dict['ulb_imgs'] = visualize(x_ulb)
        img_dict['ground truth'] = visualize_labels(y_lb)
        prediction = torch.argmax(preds, dim=1)
        img_dict['prediction'] = visualize_labels(prediction)
        return img_dict
    
    def _save_checkpoint(self, save_name, save_path, model, optimizer, epoch):
        save_filename = os.path.join(save_path, save_name)
        torch.save({'model': model.state_dict(),
                    'optimizer': optimizer.state_dict(),
                    'iter': self.iter,
                    'epoch': epoch}, save_filename)
        self.print_fn(f"model saved: {save_filename}")
        pass
